#include "network.h"

//
void CNetwork::update ()
{
	float elapsed =0;
	timeval curTime; 
	
	//ensure refresh rate 
	while (elapsed < 1000)
	{
		//
		gettimeofday (&curTime,NULL);
		elapsed = labs (curTime.tv_sec - m_lastUpdate.tv_sec ) * 1000 + 
			  (float) labs (curTime.tv_usec - m_lastUpdate.tv_usec ) / 1000;
	}

	//the code is "borrowed" from the excellent program
	//nload

	//used to access the proc network device
	FILE *file;
	//used to read from file
	char buf[512] = "";
	//copy of buf, used to chop leading characters
	char *tmp;

	//the device name, will be compared with m_device to see
	//whether we want information from this device
	std::string foundDevice;

	//open the network proc device
	file = fopen ("/proc/net/dev", "r");
	if (file == NULL)
	{
		std::cout << "cannot open /proc/net/dev !";
		return;
	}

	//overread the first two header lines (do a cat /proc/net/dev 
	//to see what i mean)
	fgets (buf, 512, file);
	fgets (buf, 512, file);

	//the pointer is used to strip the things we've already read
	//(namely to strip the device name)
	tmp = buf;

	//
	while (!feof (file))
	{
		//
		foundDevice = "";
		   
		//get the line with *all* the needed information
		fgets (buf, 512, file);
		tmp = buf;	     

		//the device name is the first bit of information in buf.
		//it is finished with a ":", so        
		//get the device name
		for (int l=0;; *tmp++, l++)
		{
			//still device name && NO leading white space
			if (buf[l] != ':' && buf[l] != ' ')
			{
				foundDevice += buf[l];
			}
			else if (buf[l] == ':')
			{		       
				//found end of device name, go on !
				//and strip the ":"
				*tmp++;
				break;			 
			}
		}//for l

		//get information from the string if this is the desired device
		if (m_device == foundDevice)
		{
			//~ std::cout << (elapsed/1000.0f)<< std::endl;
			
			//
			float up, down;
			//
			float d;
			sscanf(tmp, "%f %f %f %f %f %f %f %f %f", &down, &d, &d, &d, &d, &d, &d, &d, &up);
			//convert to kbytes
			up /= 1024.0f; down /= 1024.0f; 
			
			//calc speed the smart way : avoid to big values at
			//the beginning of the measurement
			if (m_transferDown != 0.0f)
				m_speedDown = (down - m_transferDown)/ (elapsed/1000.0f) ;
			else m_transferDown = down;
			//
			if (m_transferUp != 0.0f)
				m_speedUp   = (up   - m_transferUp)/ (elapsed/1000.0f) ;			
			else m_transferUp = up;
			
			//average speed
			m_speedDownAvg = (m_speedDown + m_speedDownAvg) / 2.0f;
			m_speedUpAvg   = (m_speedUp   + m_speedUpAvg)	/ 2.0f;
			//fastest
			if (m_speedDown > m_speedDownFastest) m_speedDownFastest = m_speedDown;
			if (m_speedUp	> m_speedUpFastest)   m_speedUpFastest	 = m_speedUp;
			//slowest
			if (m_speedDown < m_speedDownLowest)  m_speedDownLowest  = m_speedDown;
			if (m_speedUp	< m_speedUpLowest) m_speedUpLowest	 = m_speedUp;
			//save downloaded bytes
			m_transferDown += m_speedDown;
			m_transferUp   += m_speedUp;
			
			break;
		}

	}//while

	//
	fclose(file);
	
	//save function call
	gettimeofday (&m_lastUpdate, NULL);
	gettimeofday (&m_lastUpdate, NULL);
}

//
COutput output;
void CNetwork::print () const
{
	output.print (*this);
/*
	float scale = 1.0f;

	std::cout << "current speed down		: " << m_speedDown/scale << std::endl <<
		     "current speed up		: " << m_speedUp  /scale << std::endl <<		
		     "average speed down		: " << m_speedDownAvg /scale<< std::endl <<
		     "average speed up		: " << m_speedUpAvg /scale<< std::endl <<   
		     "fastest speed down		: " << m_speedDownFastest/scale << std::endl <<
		     "fastest speed up		: " << m_speedUpFastest/scale << std::endl <<
		     "lowest speed down 		: " << m_speedDownLowest/scale << std::endl <<
		     "lowest speed up			: " << m_speedUpLowest/scale << std::endl <<	    
		     "bytes transfered down		: " << m_transferDown /scale<< std::endl <<
		     "bytes transfered up		: " << m_transferUp /scale<< std::endl << std::endl;
*/
}

//
CNetwork::CNetwork (std::string device)
{
	//
	setDevice (device);
	
/*      
	//read transfer volume
	FILE* file = fopen ("transfer.alltime", "r");
	if (file)
	{
		fread (&m_transferUp, sizeof(float), 1, file);	 
		fread (&m_transferDown, sizeof(float),1,file);
	}
*/
}

CNetwork::~CNetwork ()
{
	deinit ();
}

void CNetwork::deinit ()
{
/*
	//save transfer volume to file
	FILE* file = fopen ("transfer.alltime", "w");
	if (file)
	{
		//
		fwrite ((void*)&m_transferUp, sizeof(float), 1, file);
		fwrite ((void*)&m_transferDown, sizeof(float),1, file);

		fclose (file);
	}
	else
	{
		std::cout << "!sdfljk";
	}	
*/
}

void CNetwork::setDevice (std::string device)
{
	//
	m_device = device;

	//TODO : make it dynamic
	m_speedDownMax = 96.0f; m_speedUpMax = 16.0f;
	
	//
	m_speedDown = m_speedUp = 0.0f;
	m_speedDownAvg = m_speedUpAvg = 0.0f;
	m_speedDownLowest = 0.0f; m_speedDownFastest = 0.0f;
	m_speedUpLowest = 0.0f; m_speedUpFastest = 0.0f;
	m_transferDown = 0.0f; m_transferUp = 0.0f;
	
	//save function call
	gettimeofday (&m_lastUpdate, NULL);
	m_lastUpdate.tv_sec += 1000;
}
