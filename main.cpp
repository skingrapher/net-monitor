#include <signal.h>
#include <ncurses.h>
#include <sys/unistd.h>
#include "string.h"


#include "network.h"

//quite program
void end (int n);

//command line
void parseCommand (int argc, char *argv[]);

//
CNetwork network ("eth0");

//
int main (int argc, char *argv[])
{
	//handle interrrupt signal
	signal (SIGINT, end);
	signal (SIGTERM, end);
	
	//
	parseCommand (argc, argv);
	
	
	//init ncurses
	initscr ();
	start_color();
	noecho();
	
	
	while (1)
	{
		network.update ();
		network.print ();
		sleep (1);
	}
}

//
void end (int n)
{
	//write # of bytes transfered to file
	network.deinit ();
	//end ncurses
	endwin ();
	//end program
	exit (0);
}

//
void parseCommand (int argc, char *argv[])
{
	for (int l=0;l<argc;l++)
	{
		//help ?
		if (!strcmp (argv[l], "-h") ||
		    !strcmp (argv[l], "--help"))
		{
			//
			std::cout << "[--dev device] [--max_up kb] [--max_down kb]\n";
			exit (0);
			return;
		}//-h
		//specify a different network device
		else if (!strcmp (argv[l], "--dev"))
		{
			//no device name given
			if (!argv[l+1])
			{
				std::cout << "please specify a device name, default is eth0\n";
				std::cout << "do a cat /proc/net/dev for a list of device names\n";
				exit (0);
			}
			else
			{
				network.setDevice (argv[l+1]);
			}			
		}//--dev
		//maximum upload speed
		else if (!strcmp (argv[l], "--max_up"))
		{
			network.setMaxUp (atol(argv[l+1]));
		}
		//maximum download speed
		else if (!strcmp (argv[l], "--max_down"))
		{
			network.setMaxDown (atol(argv[l+1]));
		}//--max_up
		
	}//for l
}
