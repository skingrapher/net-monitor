#include "output.h"

//
COutput::COutput ()
{
	//
	m_headerSy = 6;
	
	//dl bar
	m_barDlBg = COLOR_BLUE;
	m_barDlFg = COLOR_GREEN;
	
	//ul bar
	m_barUlBg = COLOR_GREEN;
	m_barUlFg = COLOR_GREEN;
	
	//normal text color
	m_norBg = COLOR_BLACK;
	m_norFg = COLOR_WHITE;
}

//
void COutput::print (const CNetwork& network)
{
	//
	getmaxyx (stdscr,m_sy,m_sx);
	clear (m_norFg, m_norBg);
	
	//
	printHeader (network);
	
	//
	printBars (network);
	
	//
	printSeperator (network);
	
	//
	refresh ();
}

//
void COutput::printHeader (const CNetwork& network)
{
	//
	mvprintw(0,0,"total transfered down : ");
	attrset(A_BOLD);

	//total transfered down (KB)
	if (network.m_transferDown < 1024.0f)
		printw	 ("%.2f kb",network.m_transferDown/1024.0f);
	//total transfered down (MB)
	else if (network.m_transferDown < 1024.0f*1024.0f)
		printw	 ("%.2f mb",network.m_transferDown/1024.0f);
	//total transfered down (GB)
	else 
		printw	 ("%.2f gb",network.m_transferDown/1024.0f);

	//
	attrset (0);
	mvprintw(0,m_sx-35,"total transfered up : ");
	attrset(A_BOLD);

	//total transfered up (KB)
	if (network.m_transferUp < 1024.0f)
		printw	 ("%.2f kb",network.m_transferUp/1024.0f);
	//total transfered up (MB)
	else if (network.m_transferUp < 1024.0f*1024.0f)
		printw	 ("%.2f mb",network.m_transferUp/1024.0f);
	//total transfered up (GB)
	else 
		printw	 ("%.2f gb",network.m_transferUp/1024.0f);

	//fastest down (KB)
	attrset (0);
	mvprintw(2,0,"max");
	attrset(A_BOLD);
	mvprintw(2,22,": %.2f kb",network.m_speedDownFastest);
	//avg down (KB)
	attrset (0);
	mvprintw(3,0,"avg");
	attrset(A_BOLD);
	mvprintw(3,22,": %.2f kb",network.m_speedDownAvg);		
	//currently down (KB)
	attrset (0);	
	mvprintw(5,0,"downstream");
	attrset(A_BOLD);
	mvprintw(5,22,": %.2f kb",network.m_speedDown);
	
	//fastest up(KB)
	attrset (0);
	mvprintw(2,m_sx-35,"max");
	attrset(A_BOLD);
	mvprintw(2,m_sx-15,": %.2f kb",network.m_speedUpFastest);
	//avg up (KB)
	attrset (0);
	mvprintw(3,m_sx-35,"avg");
	attrset(A_BOLD);
	mvprintw(3,m_sx-15,": %.2f kb",network.m_speedUpAvg);		
	//currently up (KB)
	attrset (0);	
	mvprintw(5,m_sx-35,"upstream");
	attrset(A_BOLD);
	mvprintw(5,m_sx-15,": %.2f kb",network.m_speedUp);

	attrset (0);
}

//
void COutput::printBars (const CNetwork& network)
{
	//
	int x = m_sx-1;
	int y = m_headerSy;
	int sx = 1;
	int sy = m_sy-m_headerSy;
	addBar (x,y,sx,sy,
		network.m_speedDownMax, network.m_speedUpMax,
		network.m_speedDown,	network.m_speedUp);
	
	float height ;
	//now print the bars
	for (int l=0;l<m_bars.size();l++)
	{
		//download bar is bigger ->draw it first
		if (m_bars[l].dlVal > m_bars[l].ulVal)
		{
			//
			drawBar (m_bars[l], true);
			drawBar (m_bars[l], false);
			
			height = m_bars[l].dlVal/m_bars[l].dlMax;
			if (height > 1.0f) height = 1.0f;
		}
		else
		{
			//
			drawBar (m_bars[l], false);
			drawBar (m_bars[l], true);
			
			height = m_bars[l].ulVal/m_bars[l].ulMax;
			if (height > 1.0f) height = 1.0f;
		}
		
		//when there is no bar, we must draw black
		init_pair(8, m_norFg, m_norBg);
		color_set(8,NULL);
		mvvline (m_bars[l].y, m_bars[l].x,
			 ' ',
			 m_sy - (height*m_bars[l].sy+m_bars[l].y));
	}
}

//
void COutput::clear (int fg, int bg)
{  
	//
	init_pair(5, fg, bg);
	color_set(5,NULL);
	
	//
	move(0,0);
	for(int l=0;l<m_sx*m_sy;l++)
		addch(' ');	
}

//
void COutput::addBar (int x,int y,int sx,int sy,
		      float dlMax, float ulMax,
		      float dlVal, float ulVal)
{
	//
	SBar bar;
	bar.x = x; bar.y = y;
	bar.sx = sx; bar.sy = sy;
	bar.dlMax = dlMax; bar.ulMax = ulMax;
	bar.dlVal = dlVal; bar.ulVal = ulVal;
	
	//avoid "speed jumps" over 25% of the bandwidth, 
	//which appear to happen *really* often
	if (m_bars.size())
	{
		float val = 0.25f;
				
		//jump in dl
		if (m_bars[m_bars.size()-1].dlVal - bar.dlVal > val)
			bar.dlVal = m_bars[m_bars.size()-1].dlVal * (1.0f-val);
		if (m_bars[m_bars.size()-1].dlVal - bar.dlVal < -val)
			bar.dlVal = m_bars[m_bars.size()-1].dlVal * (1.0f+val); 	
		//jump in ul
		if (m_bars[m_bars.size()-1].ulVal - bar.ulVal > val)
			bar.ulVal = m_bars[m_bars.size()-1].ulVal * (1.0f-val);
		if (m_bars[m_bars.size()-1].ulVal - bar.ulVal < -val)
			bar.ulVal = m_bars[m_bars.size()-1].ulVal * (1.0f+val); 	
		
		//not sure whether this helps, bars seem to be to small
		if (bar.dlVal == 0.0f)
			bar.dlVal = dlVal;
		if (bar.ulVal == 0.0f)
			bar.ulVal = ulVal;
	}
	
	//
	m_bars.push_back (bar);
	//more bars than allowed ? remove oldest
	if (m_bars.size() > m_sx/2)
		m_bars.erase (m_bars.begin ());
	
	//move bars left
	for (int l=0;l<m_bars.size();l++)
		m_bars[l].x --;
}

//
void COutput::drawBar (const SBar& bar, bool down) const
{
	float height;
	if (down)
	{
		//dl bar color
		init_pair(6, m_barDlFg, m_barDlBg);
		color_set(6,NULL);
		
		//percent height
		height = bar.dlVal/bar.dlMax;
		if (height > 1.0f) height = 1.0f;
		
		//
		mvvline (m_sy+bar.y - (bar.sy*height), //start
			 bar.x-m_sx/2,		       //pos
			 ' ',			       //char to use
			 (bar.sy*height));	       //length
	}
	else
	{
		//ul bar color
		init_pair(7, m_barUlFg, m_barUlBg);
		color_set(7,NULL);
		
		//percent height
		height = bar.ulVal/bar.ulMax;
		if (height > 1.0f) height = 1.0f;
		
		//
		mvvline (m_sy+bar.y - (bar.sy*height), //start
			 bar.x, 		       //pos
			 ' ',			       //char to use
			 (bar.sy*height));	       //length
	}
}

//
void COutput::printSeperator (const CNetwork& network)
{
	//seperator color
	init_pair(16, m_norBg, m_norFg);
	color_set(16,NULL);
	
	//draw seperator
	mvvline (m_headerSy,	//start
		 m_sx/2.0f-1.0f,//pos
		 ' ',		//char to use
		 m_sy-m_headerSy);//length

	//text color
//	  init_pair(15, m_norFg, m_norBg);
//	  color_set(15,NULL);	  
}
