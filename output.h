#ifndef OUTPUT_H
#define OUTPUT_H

#include <set>
#include <vector>
#include <ncurses.h>

#include "network.h"
class CNetwork;

//used to print the CNetwork statistics

class COutput
{
public :

	//
	COutput ();

	//plots the information from network
	void print (const CNetwork& network);

private :
	
	//prints the header information
	void printHeader (const CNetwork& network);
	//print the speed statistics
	void printBars (const CNetwork& network);
	//print the middle seperator
	void printSeperator (const CNetwork& network);

	//clears the screen to the colors COLOR_*
	void clear (int fg=COLOR_WHITE, int bg=COLOR_BLACK);

	//size of screen
	int m_sx, m_sy;
	//size of header
	int m_headerSy;

	//different colors
	//dl bar
	int m_barDlBg, m_barDlFg;
	//ul bar
	int m_barUlBg, m_barUlFg;
	//normal text color
	int m_norBg, m_norFg;

	//adds one bar, moves the others...
	void addBar (int x,int y,int sx,int sy,
		     float dlMax, float ulMax,
		     float dlVal, float ulVal);

	//one bar, including up+down
	struct SBar
	{
		//pos& (max)size of the ul bar 
		int x,y,sx,sy;
		//height
		float dlMax, dlVal;
		float ulMax, ulVal;
	};

	//draws a bar, called by printBars
	//down says which one to draw
	void drawBar (const SBar& bar, bool down) const;
	
	//
	std::vector <SBar> m_bars;
};

#endif //OUTPUT_H
