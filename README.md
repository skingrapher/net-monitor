# net-monitor

**simple bandwidth monitor**

sources from [headhunter123.funpic.de](http://headhunter123.funpic.de/net.tar.gz)

originally developed by headhunter at c-plusplus dot de

discovered on [kmandla's blog](https://kmandla.wordpress.com/2009/12/19/the-unknown-network-monitor/)

## COMPILE
To compile the bandwidth monitor enter :

    $ make
Ignore the warnings :)

## RUN
To run the bandwidth monitor enter :

    $ ./monitor

## USAGE
Quite easy : CTRL+C quits.

On the left side you see all downstream related stuff, on the right you see all the upstream stuff. Easy, isn't it ?

    --help
prints out help.

Other valid parameters are --max_up and --max_down (in kb). Default is 16 and 96kb

Thank you ! Have a lot of Fun :-)
