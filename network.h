#ifndef NETWORK_H
#define NETWORK_H

#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <string>
#include <sys/time.h>

#include "output.h"
	
//used to get information from /proc/dev/net
//basically it only retrieves the total number of 
//bytes transfered, dl&ul speed are calculated from
//these

class CNetwork
{
public :
	
	//
	friend class COutput;

	//updates the statistic
	void update ();

	//prints the statistic
	void print () const;

	//
	CNetwork (std::string device = "eth0");
	~CNetwork ();
	
	//resets the device, erasing all values saved
	void setDevice (std::string device);

	//called by dtor & end function, saves to file
	void deinit ();
	
	//
	void setMaxUp (float f) {m_speedUpMax = f;}
	void setMaxDown (float f) {m_speedDownMax = f;}

private :

	//all members are given in *kbytes*

	//maximum up&downstream
	float m_speedDownMax, m_speedUpMax;

	//current transfer speed
	float m_speedDown, m_speedUp;
	//average transfer speed
	float m_speedDownAvg, m_speedUpAvg;
	//slowest transfer speed
	float m_speedDownLowest, m_speedUpLowest;
	//fastest transfer speed
	float m_speedDownFastest, m_speedUpFastest;

	//number of bytes transfered in this session !!!
	float m_transferDown, m_transferUp;
	//number of bytes transfered since starting the measurement
	//saves to file !
	float m_transferDownAlltime, m_transferUpAlltime;

	//network device to work with (usually eth0)
	std::string m_device;

	//last time update was called
	timeval m_lastUpdate;
};

#endif //NETWORK_H
