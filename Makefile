OBJS=main.o network.o output.o

program: $(OBJS)
	g++ -lncurses -ansi -Wall $(OBJS) -o monitor
	echo -e "\a"

clean:
	rm *.o
